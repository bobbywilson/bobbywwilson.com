<?php
	
	include("../config.php");

	$page = "About";
	
	$home_status = 'class="active"';
	$home_icon = 'id="home-icon"';
	
	include("../includes/header.php");

?>



<div class="container">
	<div class="jumbotron jumbotron-home">
	
		<!-- <img src="<?php echo ABSOLUTE_PATH . 'assets/img/bobby-wilson.png'?>" alt="" class="img-self img-circle img-responsive">
		</br> -->
		<h2 class="text-center">About Me</h2></br>
		<p class="body-font">Hello, my name is Bobby Wilson and I am a Web Developer. I am a Object Oriented Programmer (OOP).
		I primarily build web systems using HTML5, CSS3, JavaScript, PHP, MySQL and Bootstrap. I am skilled at building mobile
		and responsive layouts. I also have experience with GitHub for version control, backup repository and collaboration.
		</p>
		
		<p class="body-font"> I previously worked as a Microsoft.Net Web Developer Apprentice at Eleven Fifty Consulting.
		In this role, I learned C# and the Microsoft.Net framework in order to support the consulting team.
		</p>
	
		<p class="body-font"> I am currently working as a PHP Web Developer at the University of Indianapolis.
		In this role, I use PHP, MySQL, HTML5, CSS3 and JavaScript to plan, code, test, debug, implement, maintain, and document
		web-based applications with an emphasis on secure, standards-based programming methods.
		</p>
	
		<p class="body-font"> You can see all of the projects that I have worked on by clicking the 
		<a style="color: #333 !important; text-decoration: underline !important; font-weight: bold;" href="<?php echo ABSOLUTE_PATH 
		. "portfolio/"?>">My Portfolio</a> link.
		</p>
		
	</div>

</div>





<?php
	
	include("../includes/footer.php");

?>