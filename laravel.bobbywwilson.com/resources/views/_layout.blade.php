<!DOCTYPE html>

<html lang="en">

<head>

    <title>Bobby Wilson | {{ $page }}</title>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta name="description" content="bobby w wilson, bobby wilson, bobbywwilson">
    <meta name="keywords" content="bobby w wilson, bobby wilson, bobbywwilson">
    <meta name="author" content="Bobby Wilson">
    <meta property="og:image" content="http://www.bobbywwilson.com/assets/img/bobby-wilson.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="http://www.bobbywwilson.com/assets/img/favicon.ico" />

    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('/images/ios/Icon.png') }}"/>
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('images/ios/Icon.png') }}"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('images/ios/Icon.png') }}"/>
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('images/ios/Icon.png') }}"/>
    <link rel="apple-touch-icon" sizes="72x72"   href="{{ url('images/ios/Icon.png') }}"/>
    <link rel="apple-touch-icon" href="{{ url('images/ios/Icon.png') }}"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic 700,900,900italic' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,400,700,400italic,700italic' rel='stylesheet'>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUZHRuJztZL8wgNAhl3igWyFwZSLtb8pM" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ asset(elixir('css/all.css')) }}">

    <script src="{{ asset(elixir('js/all.js')) }}"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-68574378-1', 'auto');
        ga('send', 'pageview');

    </script>

    <a class="sr-only" href="https://plus.google.com/u/0/114194462070597222604/posts" rel="author" >Google +</a>

    <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>

</head>

<body>

<nav class="navbar" id="navbar-wrapper">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="" href="{{ url('/') }}">
                <img src="{{ url('/images/bobby-wilson.png') }}" alt="Bobby Wilson" class="img-circle img-responsive logo-img-self"></a>
            <a class="navbar-brand-text" href="{{ url('/') }}">
                Bobby Wilson
            </a>
        </div>

        <div class="nav-buttons" id="nav-buttons">
            <ul class="nav navbar-nav navbar-right">
                <li class="{!! $home_status !!}" ><a href="{{ url('/') }}" >Home</a></li>
                <!--<li class="" ><a href="http://www.bobbywwilson.com/about/">About</a></li>-->
                <li class="{!! $portfolio_status !!}" ><a href="{{ url('/portfolio') }}">Portfolio</a></li>
                <li class="{!! $skills_status !!}" ><a href="{{ url('/skills') }}">Skills</a></li>
                <li class="{!! $resume_status !!}" ><a href="{{ url('/downloads/resumes/bobby-wilson-resume.pdf') }}" target="_blank">Resume</a></li>
                <li class="{!! $contact_status !!}" ><a href="{{ url('/contact') }}">Contact</a></li>
            </ul>
        </div>

        <div class="dropdown mobile-dropdown pull-right">
            <a href="" class="btn btn-default dropdown-toggle mobile-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-bars fa-2x mobile-menu"></i>
            </a>
            <ul class="dropdown-menu mobile-dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="http://www.bobbywwilson.com/">Home</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ url('/portfolio') }}">Portfolio</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ url('/skills') }}">Skills</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ url('/downloads/resumes/bobby-wilson-resume.pdf') }}" target="_blank">Resume</a></li>
                <li role="separator" class="divider"><li>
                <li><a href="{{ url('/contact') }}">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="hero-image">
    <div class="row hero-text">
        <div class="col-sm-4 col-md-4 col-lg-4"></div>

        <div class="col-sm-4 col-md-4 col-lg-4 main-hero-heading">
            <h1 class="text-center">
                <span class="text-uppercase">Bobby Wilson</span> <br/>
                <span class="text-uppercase heading-small">Web Developer</span>
            </h1>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
        </div>
    </div>
</div>
    @yield('content')
</body>

<nav class="navbar navbar-inverse" id="navbar-footer-wrapper">
    <div class="container-fluid" id="footer-container">
        <div class="navbar-footer">
            <p class="text-center">
                <a href="https://github.com/bobbywilson/Portfolio" target="_blank"><i class="fa fa-github fa-lg icon-color"></i> <span class="social-button"></span></a> <a href="https://www.linkedin.com/in/bobbywilson" class="social-button" target="_blank"><i class="fa fa-linkedin-square fa-lg icon-color"></i> <span class="social-button"></span> </a> <a href="https://www.facebook.com/bobby.wilson714" class="social-button" target="_blank"><i class="fa fa-facebook-square fa-lg icon-color"></i> <span class="social-button"></span> </a>
            </p>
            <p class="text-center">
                {!! Config::get('app.COPYRIGHT') !!}, {!! Config::get('app.CURRENT_YEAR') !!}
            </p>
        </div>
    </div>
</nav>
</html>


