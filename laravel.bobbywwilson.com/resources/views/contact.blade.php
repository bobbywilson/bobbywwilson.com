@extends('_layout')
@section('content')
<div class="container">
    <div class="jumbotron" id="nested-contact">
        <br/>

        <h2 class="text-center">{!! $page !!}</h2> <br/>

        <p class="body-font-2">Please use this form to send me a message, or contact me at {!! Config::get('app.BOBBYWILSON_PHONE') !!} or send an email to {!! Config::get('app.BOBBYWILSON_EMAIL') !!}. </p> <br/>
        <div class="bg-danger text-justify text-danger" id="client-errors-box"><p class="text-justify text-danger" class="text-muted"></p></div>

        <span id="error-return-here">
            <h5 class="text-center" id="mobile-center">Fields marked with a "<span class="required"> &bull; </span>" are required.</h5>
        </span>
        <br/>
        <div class="row">
            <div class="col-md-5" id="col-md-left">
                <div class="LI-profile-badge profile-badge"  data-version="v1" data-size="large" data-locale="en_US" data-type="horizontal" data-theme="light" data-vanity="bobbywilson"><a class="LI-simple-link" href='https://www.linkedin.com/in/bobbywilson?trk=profile-badge'>Bobby Wilson</a></div>

                <a href="https://www.google.com/maps/place/Soldiers+and+Sailors+Monument/@39.7685271,-86.158044,17z/data=!3m1!4b1!4m2!3m1!1s0x886b50bc261a1291:0x4efb96966bb31be9" target="_blank">
                    <div class="col-md-12" id="map-canvas"></div>
                </a>

            <!-- <p class="body-sub-font"><span class="glyphicon glyphicon-user"></span> Bobby Wilson<br/><span class="glyphicon glyphicon-user" id="spacer"></span> PHP Developer<br/> <span class='glyphicon glyphicon-phone'></span> (317) 682-8852<br/> <span class="email-hyperlink"><a href='mailto:php.developer@bobbywwilson.com?Subject=Bobby%20Wilson%20PHP%20Developer' target='_blank'><i class='fa fa-envelope'></i> php.developer@bobbywwilson.com</a> </span> </p> -->
            </div>

            <div class="col-md-7" id="col-md-center">
                {!! Form::open(array('url' => 'contact', 'role' => 'form-inline', 'name' => 'contact_form', 'id' => 'contact_form')) !!}
                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                    <div class="col-xs-12" id="col-xs">
                        {!! Html::decode(Form::label('first_name', 'First Name<span class="required"> &bull;</span>', $errors->has('first_name') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                        {!! Form::text('first_name', null, $errors->has('first_name') ? ['class' => 'form-control hasError', 'id' => "first_name", 'name' => 'first_name', 'placeholder' => 'i.e. John', 'onblur' => 'return upper();', 'tabindex' => 1] :
                        ['class' => 'form-control', 'id' => "first_name", 'name' => 'first_name', 'placeholder' => 'i.e. John', 'onblur' => 'return upper();', 'tabindex' => 1]) !!}
                    </div>
                    {!! $errors->has('first_name') ? '<p class="col-sm-12 text-danger" id="error-present"><b>' . $errors->first('first_name') . '</b></p>' : '' !!}
                </div>
                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                    <div class="col-xs-12" id="col-xs">
                        {!! Html::decode(Form::label('last_name', 'Last Name<span class="required"> &bull;</span>', $errors->has('last_name') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                        {!! Form::text('last_name', null, $errors->has('last_name') ? ['class' => 'form-control hasError', 'id' => "last_name", 'name' => 'last_name', 'placeholder' => 'i.e. Smith', 'onblur' => 'return upper();', 'tabindex' => 1] :
                        ['class' => 'form-control', 'id' => "last_name", 'name' => 'last_name', 'placeholder' => 'i.e. Smith', 'onblur' => 'return upper();', 'tabindex' => 2]) !!}
                    </div>
                    {!! $errors->has('last_name') ? '<p class="col-sm-12 text-danger" id="error-present"><b>' . $errors->first('last_name') . '</b></p>' : '' !!}
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <div class="col-xs-12" id="col-xs">
                        {!! Html::decode(Form::label('email', 'Email<span class="required"> &bull;</span>', $errors->has('email') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                        {!! Form::text('email', null, $errors->has('email') ? ['class' => 'form-control hasError', 'id' => "email", 'name' => 'email', 'placeholder' => 'john.smith@example.com', 'tabindex' => 1] :
                        ['class' => 'form-control', 'id' => "email", 'name' => 'email', 'placeholder' => 'john.smith@example.com', 'tabindex' => 3]) !!}
                    </div>
                    {!! $errors->has('email') ? '<p class="col-sm-12 text-danger" id="error-present"><b>' . $errors->first('email') . '</b></p>' : '' !!}
                </div>
                <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                    <div class="col-xs-12" id="col-xs">
                        {!! Html::decode(Form::label('comment', 'Comment<span class="required"> &bull;</span>', $errors->has('comment') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                        {!! Form::textarea('comment', null, $errors->has('comment') ? ['class' => 'form-control hasError', 'id' => "comment", 'name' => 'comment', 'placeholder' => 'Please enter a comment.', 'rows' => 14, 'tabindex' => 1] :
                        ['class' => 'form-control', 'id' => "comment", 'name' => 'comment', 'placeholder' => 'Please enter a comment.', 'rows' => 14, 'tabindex' => 4]) !!}
                    </div>
                    {!! $errors->has('comment') ? '<p class="col-sm-12 text-danger" id="error-present"><b>' . $errors->first('comment') . '</b></p>' : '' !!}
                </div>
                <div class="form-group" id="submit-button-box">
                    <div class="col-xs-12" id="col-xs">
                        <label class="sr-only" for="submit">Submit</label>
                        <button type="submit" class="btn btn-default btn-lg" id="submit" name="submit" onsubmit="validateForm()" tabindex="5">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<hr class="hr">
@endsection