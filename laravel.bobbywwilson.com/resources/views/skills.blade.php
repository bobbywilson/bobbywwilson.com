@extends('_layout')
@section('content')
    <div class="container home-container">
        <div class="jumbotron jumbotron-home">
            <div class="row code-wrapper">

                <h2 class="text-center">Front-End Skills</h2> <br/><br/>

                <div class="col-sm-4 col-md-4 col-lg-4 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>HTML5</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><i class="fa fa-html5 fa-5x-html5"></i></div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>CSS3</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><i class="fa fa-css3 fa-5x-css3"></i></div>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>JavaScript</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><img src="{{ url('/images/js-logo.jpg') }}" alt="" class="fa-5x-js"></div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>LESS</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust-less"><img src="{{ url('/images/less.png') }}" alt="" class="fa-5x-less"></div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>Bootstrap</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><img src="{{ url('/images/bootstrap.png') }}" alt="" class="fa-5x-bootstrap"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container home-container pull-up">
        <div class="jumbotron jumbotron-home">
            <div class="row code-wrapper">
                <h2 class="text-center">Back-End Skills</h2> <br/><br/>
                <div class="col-sm-6 col-md-6 col-lg-6 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>PHP</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><img src="{{ url('/images/php.jpg') }}" alt="" class="fa-5x-js"></div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>MySQL</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><img src="{{ url('/images/mysql.jpg') }}" alt="" class="fa-5x-js"></div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2 text-center code-logo">
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>Laravel</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon top-padding-adjust"><img src="{{ url('/images/laravel.png') }}" alt="" class="fa-5x-laravel"></div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>C#</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon"><img src="{{ url('/images/c-sharp.png') }}" alt="" class="fa-5x-c-sharp"></div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>MS SQL</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon"><img src="{{ url('/images/mssql.png') }}" alt="" class="fa-5x-mssql"></div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2 text-center code-logo">
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8 text-center code-logo">
                    <div class="row text-left code-logo-heading"><h4>Visual Studio</h4><i class="fa fa-check-circle circle-icon pull-right"></i></div>
                    <div class="row code-logo-icon"><img src="{{ url('/images/visualstudio.png') }}" alt="" class="fa-5x-mssql"></div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2 text-center code-logo"></div>
            </div>
            <br/><br/>
        </div>
    </div>

    <hr class="hr">
@endsection