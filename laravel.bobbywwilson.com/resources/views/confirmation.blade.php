@extends('_layout')
@section('content')
    <div class="jumbotron" id="main">
        <div class="container-fluid confirmation-box">
            <h2>{{ $page }}</h2><br/>

            <h4> Thank you for your comment.</h4>

            <h4>{{ $contact->first_name . " " . $contact->last_name }}</h4>

            <h4>
                <a href="mailto:{!! Config::get('app.BOBBYWILSON_EMAIL_NO_ICON') !!}" title = "Email Address: Bobby Wilson" class='response-text'>{!! Config::get('app.BOBBYWILSON_EMAIL_NO_ICON') !!}</a>
            </h4>

            <p id="normal-justify-text">{{ htmlspecialchars($contact->comment, ENT_QUOTES, 'utf-8') }}</p>
            <p class="confirmation" id=""> <b>Submitted: Saturday, {{ date('l, F, d, Y') }}</b></p><br/>
            <h4 class="mobile-nav-return">Return To</h4><br/>
                <p class="mobile-nav-return" id="normal-justify-text">
                    <a href="http://www.bobbywwilson.com/">Home</a> |
                    <a href="http://www.bobbywwilson.com/portfolio/">Portfolio</a> |
                    <a href="http://www.bobbywwilson.com/skills/">Skills</a> |
                    <a href="http://www.bobbywwilson.com/contact/">Contact</a><br/><br/>
                </p>
        </div>
    </div>

    <hr class="hr">
@endsection