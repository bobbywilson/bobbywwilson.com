<!DOCTYPE html>

<html lang="en">

<head>

    <title>Bobby Wilson | Home</title>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta name="description" content="bobby w wilson, bobby wilson, bobbywwilson">
    <meta name="keywords" content="bobby w wilson, bobby wilson, bobbywwilson">
    <meta name="author" content="Bobby Wilson">
    <meta property="og:image" content="http://www.bobbywwilson.com/assets/img/bobby-wilson.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="http://www.bobbywwilson.com/assets/img/favicon.ico" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic 700,900,900italic' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,400,700,400italic,700italic' rel='stylesheet'>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUZHRuJztZL8wgNAhl3igWyFwZSLtb8pM" type="text/javascript"></script>

    {{--<link rel="stylesheet" href="{{ $message->embed(asset('css/all.css')) }}">--}}


    <style>
        body, html {
            padding: 0 !important;
            margin: 0px !important;
        }
        #navbar-footer-wrapper	{
            background-color: #000;
            border-radius: 0px;
            position: relative !important;
            bottom: 0px !important;
            margin-bottom: 0px !important;
            color: #eff0f2 !important;
            width: 100%;
            height: 150px;
            opacity: 0.80;

        }
        .navbar-footer	{
            margin-top: 0%;
            margin-right: auto;
            margin-left: auto;
            width: 230px;
            height: 60px;
            border: 0px solid #eff0f2;
        }
        #footer-container	{
            margin-top: 2%;
            border: 0px solid #eff0f2;
        }
        .navbar-header {
            opacity: 0.80 !important;
            width: 200px;
            height: 60px !important;
            margin-top: 5px;
            margin-left: 40px !important;
            padding: 10px !important;
        }
        .navbar-header .navbar-brand-text {
            font-family: "Roboto Condensed", sans-serif !important;
            font-weight: bold;
            font-size: 20px !important;
        }
        #navbar-wrapper	{
            background-color: #eff0f2 !important;
            border-radius: 0px;
            position: absolute;
            top: 5px !important;
            padding: 0 10px 0 10px !important;
            width: 100%;
            height: 60px !important;
            z-index: 5 !important;
        }
        #navbar-wrapper a	{
            font-family: "Roboto Condensed", sans-serif;
            font-weight: bold;
            font-size: 15px;
            border-radius: 0px;
            color: #000 !important;
            z-index: 5 !important;
        }
        .navbar-brand	{
            color: #eff0f2 !important;
            font-size: 100% !important;
            border: 1px solid #c00;
        }
        .navbar-brand-logo	{
            color: #eff0f2 !important;
            font-size: 100% !important;
            border: 0px solid #333;
            width: 56px;
            height: 60px;
        }
        #navbar-wrapper .navbar-right a:hover	{
            opacity: 0.80 !important;
            color: #333 !important;
            background-color: #eff0f2 !important;
            text-decoration: none !important;
        }
        .dropdown-menu a:hover	{
            opacity: 0.80 !important;
            color: #333 !important;
            background-color: #eff0f2 !important;
            text-decoration: none !important;
        }
        .img-self	{
            display: block;
            margin-left: auto;
            margin-right: auto;
            border: 2px solid #000;
            z-index: -1 !important;
        }
        .logo-img-self	{
            position: absolute;
            top: 5px;
            left: 10px;
            height: 50px !important;
            width: auto !important;
            border: 2px solid #000;
            z-index: -1 !important;
        }
        .hero-image {
            background: url('http://bobbywwilson.com/assets/img/hero.jpg') no-repeat;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-color: #333 !important;
            padding-left: 20px !important;
            padding-right: 20px !important;
            padding-bottom: 40%;
        }
        .linkedin-image {
            background: url('http://bobbywwilson.com/assets/img/linkedin-image.png') no-repeat;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-color: #333 !important;
            padding-left: 20px !important;
            padding-right: 20px !important;
            padding-bottom: 65%;
            margin-top: 0px;
            border: 2px solid #ccc;
        }
        .hero-text	{
            position: relative;
            top: 100px !important;
            margin-left: auto;
            margin-right: auto;
            border: 0px solid #333 !important;
            width: 95%;
        }
        .main-hero-heading {
            border: 1px solid #eff0f2;
            border-radius: 5px;
            margin-left: auto;
            margin-right: auto;
            padding: 0px !important;
        }
        .main-hero-heading h1 {
            color: #eff0f2;
            letter-spacing: 4px;
            font-family: "Roboto Condensed", sans-serif;
            font-weight: normal;
        }
        .heading-small {
            color: #eff0f2 !important;
            font-size: 20px !important;
        }
        #social	{
            text-decoration: none;
            z-index: 4 !important;
            color: #428BCA !important;
        }
        .social-nav-buttons	{
            border: 1px solid #c00 !important;
            margin-left: auto !important;
            margin-right: auto !important;
            width: 100% !important;
        }
        .social-button {
            font-size: 15px;
            font-weight: bold;
            color: #eff0f2 !important;
        }
        .icon-color {
            color: #eff0f2 !important;
            font-size: 40px !important;
            padding: 10px !important;
        }
        .jumbotron {
            border: 0px solid green !important;
            background-color: #eff0f2;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }
        .body-margin {
            position: relative;
            top: 50px;
            height: 50px;
            padding-top: 20px !important;
        }
        main {
            background-color: #eff0f2;
        }
    </style>

    <script src="{{ asset(elixir('js/all.js')) }}"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-68574378-1', 'auto');
        ga('send', 'pageview');

    </script>

    <a class="sr-only" href="https://plus.google.com/u/0/114194462070597222604/posts" rel="author" >Google +</a>

    <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>

</head>

<body>

<div class="body-margin"></div>

<main>
    <nav class="navbar" id="navbar-wrapper">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="" href="{{ url('http://bobbywwilson.com') }}">
                    <img src="http://bobbywwilson.com/assets/img/bobby-wilson-pro.png" alt="Bobby Wilson" class="img-circle img-responsive logo-img-self"></a>
                <a class="navbar-brand-text" href="{{ url('/') }}">
                    Bobby Wilson
                </a>
            </div>
        </div>
    </nav>

    <div class="hero-image">
        <div class="row hero-text">
            <div class="col-sm-4 col-md-4 col-lg-4"></div>

            <div class="col-sm-4 col-md-4 col-lg-4 main-hero-heading">
                <h1 class="text-center">
                    <span class="text-uppercase">Bobby Wilson</span> <br/>
                    <span class="text-uppercase heading-small">Web Developer</span>
                </h1>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4"></div>
        </div>
    </div>

    @yield('content')
</main>
</body>

<nav class="navbar navbar-inverse" id="navbar-footer-wrapper">
    <div class="container-fluid" id="footer-container">
        <div class="navbar-footer">
            <p class="text-center">
                <a href="https://github.com/bobbywilson/Portfolio" target="_blank"><i class="fa fa-github fa-lg icon-color"></i> <span class="social-button"></span></a> <a href="https://www.linkedin.com/in/bobbywilson" class="social-button" target="_blank"><i class="fa fa-linkedin-square fa-lg icon-color"></i> <span class="social-button"></span> </a> <a href="https://www.facebook.com/bobby.wilson714" class="social-button" target="_blank"><i class="fa fa-facebook-square fa-lg icon-color"></i> <span class="social-button"></span> </a>
            </p>
            <br/>
            <p class="text-center">
                <span class='glyphicon glyphicon-copyright-mark'></span> Copyright Bobby Wilson, 2017
            </p>
        </div>
    </div>
</nav>
</html>


