@extends('_email-layout')
@section('content')
<div class="jumbotron" id="main">
    <h2>{!! $confirmation_notice !!}</h2>
    <h3 class='text-justify'>{!! $contact->first_name !!} {!! $contact->last_name !!}</h3>
    <p><b>{!! Html::mailto($contact->email, $contact->email, array('class' => 'email-confirmation-link')) !!} </b><br/> </p>
    <p class="">{!! nl2br($contact->comment) !!}</p>
    <p class=""> <b>Submitted: {!! date("F, d, Y", strtotime($contact->created_at)) . " at " . date("h:i A", strtotime($contact->created_at)) !!} </b></p>
</div>
<a class="" href="{{ url('https://www.linkedin.com/in/bobbywilson/') }}">
    <div class="linkedin-image"></div>
</a>
@endsection
