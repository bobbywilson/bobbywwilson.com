@extends('_email-layout')
@section('content')
    <div class="jumbotron" id="main">
        <div class="container-fluid">
            <div class="jumbotron" id="nested-contact">
                <h2>{!! $confirmation_notice !!}</h2>
                <h3 class='text-justify'>{!! $contact->first_name !!} {!! $contact->last_name !!}</h3>
                <p><b>{!! Html::mailto($contact->email, $contact->email, array('class' => 'email-confirmation-link')) !!} </b><br/> </p>
                <p class="text-justify">{!! nl2br($contact->comment) !!}</p>
                <p class="text-justify"> <b>Submitted: {!! date("F, d, Y", strtotime($contact->created_at)) . " at " . date("h:i A", strtotime($contact->created_at)) !!} </b></p>
            </div>
        </div>
    </div>
    <div class="linkedin-image"></div>
@endsection
