const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
    'resources/assets/js/javascript.js'
], 'public/js/all.js');

mix.combine([
    'resources/assets/css/stylesheet.css',
    'resources/assets/css/responsive.css'
], 'public/css/all.css');