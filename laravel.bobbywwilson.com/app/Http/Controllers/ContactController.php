<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact() {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = "Contact";
        $home_status = "";
        $portfolio_status = "";
        $skills_status = "";
        $resume_status = "";
        $contact_status = "active-heading";

        return view('contact', compact('error_message', 'page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validation(Request $request)
    {

        $page = "Contact";
        $confirmation_notice = "Contact Notice";
        $home_status = "";
        $portfolio_status = "";
        $skills_status = "";
        $resume_status = "";
        $contact_status = "active-heading";

        $validation = Validator::make(
            $request->all(),
            $this->contactValidation['rules'],
            $this->contactValidation['messages']
        );

        $first_name = ucwords(filter_var(trim($request['first_name']), FILTER_SANITIZE_STRING));
        $last_name = ucwords(filter_var(trim($request['last_name']), FILTER_SANITIZE_STRING));
        $email = strtolower(filter_var(trim($request['email']), FILTER_SANITIZE_EMAIL));
        $comment = filter_var(trim($request['comment']), FILTER_SANITIZE_STRING);

        $error_message = collect();

        filter_var($email, FILTER_VALIDATE_EMAIL)
            ? true
            : $error_message->push("Email must be formatted as follows: john.smith@example.com.");

        $first_name == "" || $first_name === null || preg_match('[\d]', $first_name)
            ? $error_message->push("First name is required and must be letters only.")
            : false;

        $last_name == "" || $first_name === null || preg_match('[\d]', $last_name)
            ? $error_message->push("Last name is required and must be letters only.")
            : false;

        $email == "" || $email === null
            ? $error_message->push("Email is required.")
            : false;

        $comment == "" || $comment === null
            ? $error_message->push("Comment is required.")
            : false;

        strlen($comment) < 50 || strlen($comment) > 500
            ? $error_message->push("Comment must be between 50 and 500 characters.")
            : false;

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else if ($error_message->isEmpty() == false) {
            return view('contact', compact('error_message', 'page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
        } else {

            try {
                $contact = new Contact();

                $contact->first_name = $first_name;
                $contact->last_name = $last_name;
                $contact->email = $email;
                $contact->comment = $comment;

                $contact->save();

                $data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'comment' => $comment,
                    'email' => $email
                );

                Mail::send('admin.email.admin-comment-notification',
                    [
                        'contact' => $contact,
                        'page' => $page,
                        'confirmation_notice' => $confirmation_notice,
                        '$home_status' => $home_status = 'class=""',
                        '$portfolio_status' => $about_status = 'class=""',
                        '$skills_status' => $property_status = 'class=""',
                        '$resume_status' => $resident_status = 'class=""',
                        'contact_status' => $contact_status = 'active-heading'

                    ], function($message) use ($data)
                    {
                        $admin_email = 'bobby.wilson@bobbywwilson.com';
                        $admin_name = 'Bobby Wilson';
                        $from_email = 'info@bobbywwilson.com';
                        $from_name = 'Bobby Wilson';
                        $subject =  'Bobby Wilson';

                        $message->to($admin_email, $admin_name);
                        $message->from($from_email, $from_name);
                        $message->subject($subject);
                    });

                Mail::send('email.sender-comment-notification',
                    [
                        'contact' => $contact,
                        'page' => $page,
                        'confirmation_notice' => $confirmation_notice,
                        '$home_status' => $home_status = 'class=""',
                        '$portfolio_status' => $about_status = 'class=""',
                        '$skills_status' => $property_status = 'class=""',
                        '$resume_status' => $resident_status = 'class=""',
                        'contact_status' => $contact_status = 'active-heading'
                    ], function($message) use ($data)
                    {
                        $sender_email = $data['email'];
                        $sender_name = $data['first_name'] . " " . $data['last_name'];
                        $from_email = 'info@bobbywwilson.com';
                        $from_name = 'Bobby Wilson';
                        $subject =  'Bobby Wilson';

                        $message->to($sender_email, $sender_name);
                        $message->from($from_email, $from_name);
                        $message->subject($subject);
                    });

            } catch (Exception $error) {
                Log::debug('Showing user profile for user: ',
                    array_merge($this->logVariables, $request->all()));
            }

            return view('confirmation', compact('contact', 'request_collection', 'page', 'confirmation_notice', 'error_message', 'page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
        }
    }


    public function show_email()
    {
        $page = "Contact";
        $confirmation_notice = "Contact Notice";
        $home_status = "";
        $portfolio_status = "";
        $skills_status = "";
        $resume_status = "";
        $contact_status = "active-heading";

        return view('email/sender-comment-notification', compact('page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
