<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = "Home";
        $home_status = "active-heading";
        $portfolio_status = "";
        $skills_status = "";
        $resume_status = "";
        $contact_status = "";

        return view('index', compact('page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
    }

    public function portfolio()
    {
        $page = "Portfolio";
        $home_status = "";
        $portfolio_status = "active-heading";
        $skills_status = "";
        $resume_status = "";
        $contact_status = "";

        return view('portfolio', compact('page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
    }

    public function skills()
    {
        $page = "Skills";
        $home_status = "";
        $portfolio_status = "";
        $skills_status = "active-heading";
        $resume_status = "";
        $contact_status = "";

        return view('skills', compact('page', 'home_status', 'portfolio_status', 'skills_status', 'resume_status', 'contact_status'));
    }
}
