<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $contactValidation = [
        'rules'    => [
            'first_name'    =>  'required|alpha',
            'last_name'     =>  'required|alpha',
            'email'         =>  'required|email',
            'comment'       =>  'required|between:50,500',
        ],
        'messages' => [
            'first_name.required'       =>  'First name is required.',
            'first_name.alpha'          =>  'First name must be letters only.',
            'last_name.required'        =>  'Last name is required.',
            'last_name.alpha'           =>  'Last name must be letters only.',
            'email'                     =>  'Email is required.',
            'email.email'               =>  'Email must be in the following format: john.smith@example.com.',
            'comment.required'          =>  'Comment is required.',
            'comment.between'           =>  'Comment is required and must be between 50 and 500 characters.',
        ],
    ];
}
